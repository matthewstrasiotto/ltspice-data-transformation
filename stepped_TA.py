# -*- coding: utf-8 -*-
"""
Created on Mon May 21 21:04:02 2018

@author: cstra
"""

import Transient_Analysis as ta
import pandas as pd

def parse_string(string):
    out = ta.build_parser(string.split())
    return out

def read_stepped_file(filename):
    
    df = pd.read_csv(filename, engine='python',delimiter='\t+')
    intervals = df[df.iloc[:,1].isna()]
    
    intervals = intervals.iloc[:,0]
    intervals = intervals.str.replace('Step information:\s+','',case=False)
    
    strings = intervals.str.extractall(r'(?P<Name>\S+)=(?P<Value>\S+)')
    
    strings.columns = strings.columns.rename('type')
    
    strings = strings.unstack(level='match')
    strings = strings.reorder_levels(['match','type'], axis='columns').sort_index(axis='columns')
    
    idx= pd.IndexSlice
    
    name_match = {}
    names = strings.xs('Name', level='type', axis='columns')
    for col in names.columns:
        for name in names[col].unique():
            name_match[name] = col
    
    
    out = pd.DataFrame(index=strings.index, columns=name_match.keys())
    
    for col in out.columns:
        out[col] = strings.loc[:, idx[name_match[col], 'Value']]
    
    df = (df
          .join(out)
          .fillna(method='ffill')
          .drop(index=out.index)
          .reset_index(drop=True)
          .rename_axis('idx',axis='index')
          .set_index(list(out.columns), append=True)
          .reorder_levels(list(out.columns) + ['idx'],axis='index')
          )
    
    return df

    
    
def read_dc_op(filename):
    out = pd.read_csv(filename, engine='python', delimiter='\t+', header=None, skiprows=2, names=['Value','Unit'])
    return out


