# -*- coding: utf-8 -*-
"""
Created on Mon May 21 17:34:11 2018

@author: Matthew Strasiotto
"""
import sys, argparse
import pandas as pd
import numpy as np

unary_ops = {'RMS'  : lambda x: RMS(x),
             'AMP'  : lambda x: VPP(x)/2,
             'PP'   : lambda x: VPP(x)}

binary_ops= {'/'    : lambda x: div(x)}
def RMS(x): return np.sqrt(np.average(x*x))
def PP(x): return (np.max(x) - np.min(x))/2
def div(x):
    for item, next_item in zip(x, x[1:]):
        return item / next_item
def main(args):
    
    frame = pd.read_csv(args.file, delimiter='\t+')
    
    if args.colnames:
        frame = rename_cols(frame, args.colnames)
    
    frame = interact(frame)
    return frame
    
    
def build_parser(argv):
    parser = argparse.ArgumentParser(prog='Transient Analysis Script')
    parser.add_argument("-f","--file", help="<Required> Filename to load", required=True)
    parser.add_argument("-c","--colnames",help="Labels for each column", nargs='+', type=str)
    parser.add_argument("-o","--output", help = "Name of output file", default="out.csv")
    args = parser.parse_args(argv)
    return args

if __name__ == "__main__":
    args = build_parser(sys.argv)
    df = main(args)
    df.to_csv(args.output)
    
def interact(frame):
    frame = frame.copy()
    while True:
        column = input('Choose column:\n\t' + '\n\t'.join(frame.columns) + '\n' + 'or \tQUIT\n').split('|')
        if not column or column[0] == 'QUIT': 
            break
        op = input('Operation: ')
        
        if op in unary_ops:    
            frame[column[0]+'_'+op] = unary_ops[op](frame[column[0]])
        elif op in binary_ops:
            frame[column[0]+'op'+column[1]] = frame[column].apply(binary_ops[op], axis='columns', reduce=True)
    return frame


def rename_cols(frame, args):
        for old_col, new_col in zip(frame.columns,args.colnames):
            frame = frame.rename(columns={old_col : new_col})
        return frame